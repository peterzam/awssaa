# IAM

---

## User

---

## Policies
- Multiple Policies can attach directly to single user.
- Version, Id, Statement
    - Statement - Sid, *Effect, Principal, *Action, *Resource, Condition

---

## Groups
- Groups cannot be in Groups.
- Multiple groups can attach to single user.

---

## Roles
- AWS Services
    - EC2 instance Roles
    - Lambda
    - Or other services
- Another AWS account
- Web Identity(Cognito or any OpenID provider)
- SAML 2.0 federation

---

# Identity Providers
- To manage your user identities outside of AWS, but grant the use identities permissions to use AWS resources in your account
- AWS suggest to use Single Sign-On (SSO)
- Using SAML & OpenID Connect

---

## CloudShell

---

## Access reports
- Access analyzer
- Credential report
- Organization activity
- Service Control Policies (SCPs)

---