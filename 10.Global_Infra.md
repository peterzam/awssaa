# Global Infra
- Multiple Geographies
- Decreased Latency
- Disaster Reovery
- Attack Protection

---

## Apps
- Route 53
- CloudFront
- S3 Transfer Acceleration
- Global Accelerator

---

## Route 53
- Managed DNS
- Routing Policy
    - Simple Routing Policy
    - Weighted Routing Policy
    - Latency Routing Policy
    - Failover Routing Policy

---

## S3 Transfer Acceleration
- S3 to Edge, Edge to S3

---

## Outposts
- Server racks setup by AWS on prem
- Client responsible for outpost security
- Easier migration
- Fully managed service
    
---

## Wavelength
- Zone/Edge
- 5G networks with AWS service
- AWS edge at 5G telecoms
