# Cloud monitoring

## CloudWatch Metric
- Billing Metric (only in us-east-1)
- EC2 instances
    - update every 5 min
- EBS volumes
- S3 buckets
- Service Limits

### CloudWatch Custom metrics
- Use API call PutMetricData

### CloudWatch Dashboard
- Dashboard are global
- Can include graphs from different AWS accounts and regions
- Can change timezone & time range of the dashboard
- Can setup automatic refresh
- Can share with outsiders (public, email, 3rd party SSO provider through Amazon Cognito)

---

## CloudWatch Alarms
- Alarm actions
    - Auto Scaling
    - EC2 Actions
    - SNS noti

- Various options (sampling, %, max, min, etc)
- Billing Alarm

---

## CloudWatch Logs
- Collect log
    - Elastic Beanstalk
    - ECS
    - Lambda
    - CloudTrail
    - CloudWatch log agents : on EC2 or on-premises servers
    - Route 53: Log DNS queries
- Real-time monitoring
- Adjustable logs retention
- Log Stream
- Log groups

---

## CloudWatch Event
- Schedule: CRON jobs
- Event Pattern
- JSON payload is passed to target
---

## EventBridge
- Evolution of CloudWatch Event
- Default event bus (from AWS)
- Partner event bus (from Zendesk, DataDog, Segment, Auth0)
- Custom event bus (from own custom apps)
- Schema Registry
- CloudWatch Event will rename as EventBridge

---

## CloudTrail
- Provide governance, compliance and audit AWS accounts
- Enabled default

- Get an history of events/API within AWS account
    - Console
    - SDK
    - CLI
    - AWS Services

- Can put CloudTrail logs into CloudWatch Logs or S3

- 3 kinds of events
    - Management Events - Operations performed in AWS accounts
    - Data Events - By default not logged, S3 object-level activity, Lambda functions
    - CloudTrail Insight Event
        - Enable Insights to detect unusual activity
    
- Events are stored for 90 days

### CloudTrail Event Retention
- Event are stored for 90 days default
- To keep events beyond, log them to S3 and use Athena

---

## X-Ray
- Debugging in Production
    - Test lcoally
    - Add log statements
    - Re-deploy in production

- Tracing and visual analysis of apps
- Easy troubleshooting
- Pin point service issues
- Review request behavior

---

## CodeGuru
- ML-powered automated code review & app performance recommendations

- Two functionalities

    - CodeGuru Reviewer 
        - automated code reviews for static code analysis (development)
        - Supports Java & Python
        - Integrates with Github, Bitbucket & CodeCommit

    - CodeGuru Profiler 
        - recommendations about app performance in runtime (production)
        - Identify code inefficients
        - Improve app performance
        - Provice heap summary
        - Decrease compute costs
        - Anomaly Detection
        - Both on AWS & on-premise

---

## Status
- Service Health Dashboard
- Has RSS feed
- Historical info

---

## Personal Health Dashboard
- Alerts and remediation guidance on AWS events that may impect you
- Personalized view into performance & availability of AWS
- Display relevant and timely info
