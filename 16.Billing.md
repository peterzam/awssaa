# Billing

## Organization
- Global service
- Allow to manage multiple AWS accounts
- Main account = master account, others = child account

- Cost Benifits:
    - Consolitdated Billing across all
    - Aggregated usage
    - Pooling Reserved EC2

- Has API to automate AWS account creation
- Restrice account privileges using Service Control Policies(SCP)

- Multi Account Strategies
    - Accounts per department, per cost based on regulatory restriction (using SCP)
    - Better resource isolation (e.g. VPC)
    - Have separate per-account service limits
    - Have seperate logging

- Should use CloudTrail and CloudWatch Log to central S3 account

- Organization Units (OU) - Account groups

- Service Control Policies (SCP)
    - Whitelist or blacklist IAM actions
    - Applied at OU or Account level
    - Does not apply to Master Account
    - Applied to all Users & Roles of the Account, including Root(but cannot be stopped cuz Master)
    - Use Cases:
         - Restrict access to certain services (e.g can't use EMR)
         - Enforce PCI compliance by explicitly disabling services

---

## Control Tower
- Set up & govern a secure & compliant multi-account
- Run on top of Organizaions
- Implement SCPs

---

## Pricing Models
- 4 models
    - Pay as you go
    - Save when you reserved
    - Pay less by using more
    - Pay less as AWS grows

- Compute - EC2
    - On-demand
    - Reserved
    - Spot
    - Dedicated
    - Saving plans

- Compute - Lambda & ECS
    - Lambda - Pay per call
    - ECS - Pay for resource/EC2
    - Fargte - Pay for vCPU, RAM, Storage

- Storage - S3
    - Different for different storage class
    - Number & size of objects (based on volume)
    - Number & type of requests
    - Data transfer OUT
    - S3 Transfer Acceleration
    - Lifecycle transitions
    - Similar service : EFS

- Storage - EBS
    - Pay per volume (provisionned)
    - IOPS:
        - General Purpose SSD
        - Provisioned IOPS SSD
        - Magnetic
    - Snapshots:
    - Data transfer - OUT only

- Database - RDS
    - Pay per hour & additional storage
    - DB characteristics : Engine, Size, Memory class
    - Purchase type: 
        - On-demand, Reserved
    - Backup Storage : No additional charge till 100% of total database storage
    - Data transfer - OUT only
    - Deployment type

- CDN - CloudFront
    - Data transfer - OUT only
    - Pay per GB, requests

- Networking
    - Free for private in same AZs
    - Cost for Public IP/Elastic IP
    - Cost for different AZs and Regions

---

## Saving Plan
- Long-term commitment (e.g 'commit $10/h for 1 yr') 
- EC2 Savings Plan
    - Commit to usage.
    - Upfront
- Compute Saving Plan
    - EC2, Fargate, Lambda

---

## Compute Optimizer
- Reduce cost and improve performance by recommending
- Uses ML and CloudWatch metric to recommend
- EC2, EC2 ASG, ESB, Lambda function

---

## Billing & Costing Tools
- Estimating costs in cloud
    - Total Cost of Ownership(TCO) Calculator
        - Pay-as-you-go model
        - Estimate the cost savings
        - Can be used in presentations
        - Compare on-premises vs AWS cloud
    - Pricing Calculator
        - Estimate the total cost for architecture

- Tracking costs in cloud
    - Billing Dashboard
    - Cost Allocation Tags
        - Track cost detail
        - AWS generated tags or User generated custom tags
        - Tags can be used to create Resource Group
    - Report
        - The most comprehensice data
        - All AWS usage in hourly/daily pattern
        - Can be used in Athena, Redshift, QuickSight
        - CSV format
    - Cost Explorer
        - Can forecast usage up to 12 months
        - Hourly & resourse level
        - Saving plan recommendation
- Monitor against costs plans
    - Billing data metric is stored in CloudWatch us-east-1
    - For overall worldwide
    - ### AWS Budgets
        - Send alarm when budget exceeds
        - Usage, Cost, Reservation, Saving Plans
        - For Reserved Instances - Track utilization, EC2, ElasticCache, RDS & Redshift
        - Up to 5 SNS notifications per budget
        - 2 budgets free, $0.02/day/budget

---

## Trusted Advisor
- Analyze AWS account and provice recommendation
    - Cost Optimization, Performance, Security, Fault Tolerance, Service Limits
- Normal Trusted Advisor - Core checks and recommendations
- Full Trusted Advisor 
    - Business & Enterprise
    - Programmatic Access using AWS Support API

---

## Support Plan
- Basic Support : Free
    - Customer Service & Communities
    - Trusted Advisor
    - Personal Health Dashboard
- Developer
    - Business hours email access
    - Case Severity / response times:
        - General guidance: < 24 business hours
        - System inpaired: < 12 business hours
    - Provides general architectural guidance
- Business
    - Trust Advisor - Full set checks + API access
    - 24/7 phone, email and chat access
    - Access to Infrastucture Event Management for additional fee
    - Case Severity / response times:
        - General guidance: < 24 business hours
        - System inpaired: < 12 business hours
        - Production system imapired: < 4 hours
        - Production system down: < 1 hour
- Enterprise
    - Technical Account Manager(TAM)
    - Concierge Support Team (for billing & best practices)
    - Infrastructure Event Management, Well-Architected & Operations Reviews
        - Case Severity / response times:
        - General guidance: < 24 business hours
        - System inpaired: < 12 business hours
        - Production system imapired: < 4 hours
        - Production system down: < 1 hour
        - Business-critical system down: < 15 min
