# EC2 - Elastic Compute Cloud = infrastructure as a Service
- Store data - EBS 
- Network attach - EFS
- Load balance - EBS
- Auto-scaling group- ASG
- Setup script (Bootstrap script/User data) 
    - run once 
    - first start 
    - root.

---

## EC2 Instance Types
- Main categories
    - General Purpose
    - Compute Optimized
    - Memory Optimized
    - Storage Optimized

---

## Security Groups
- Only allow allow rules
- reference by IP or Security group (Security in security group)
- Locked to region like VPC

---

## Purchasing Options
- On Demand
    - Up to 72% discount
    - Linux - bill per second after first 1min, All other - bill per hour

- Reserved
    - Reversed- up to 75% discount
    - Convertible - up to 54% discount
    - Scheduled

- Spot 
    - Up to 90% discount
    - Max spot price
        - Max spot price - if over 2 min grace period
    - Spot Block - block interrupt 1 - 6 hours

- Spot Fleet
    - Set of Spots + optional On-Demand
    - Create spots automatically until max cost or defined capacity
    - Strategies
        - lowestPrice
        - diversified - distributed across all pools
        - capacityOptimized - optimal capacity number of instances

- Dedicated Hosts
    - Unmanaged Dedicated Server
    - compliance rerquirements
    - existing server-bound software license
    - allocate for 3 year / On demand 
    - Bring Your Own License(BYOL)
      
- Dedicated Instances
    - Managed Dedicated Server

---

## EC2 Userdata script
- Bootstrap instances using bash script
- Run as root User

---

## EC2 Nitro
- New versions of EC2 with new type of virtualization
- A combination of dedicated hardware and lightweight hypervisor
- Faster and enhanced security
- Nitro cards, Nitro security chip, Nitro Hypervisor

### Nitro Enclave
- Main usage of nitro
- Seperated virtualized nitro type VM
- Isolated compute environments for security & data integrity
- Built in integration in KMS and ACM(AWS certificate manager)
- Independent kernel
