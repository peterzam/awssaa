# Serverless
- AWS Lambda
- S3, Athena/DynamoDB & QuickSight, Fargate, Lambda

---

## Lambda
- Used code functions
- Limited by time
- Run on-demand
- Scaling is automated
- Pay per request & compute time
- Event-Driven: invoded by AWS
- Monitering through CloudWatch
- Only Lambda Container Image can run
- For CRON job, use with CloudWatch Events/EventBridge
- Lambda@Edge is a feature of CloudFront that lets you run code closer to users

---

## Lambda Limits
- Execution
    - Mem alloc 128MB - 10GB (1MB increment)
    - 900s (15mins)

---

## DyanmoDB
- NoSQL
- Made of Tables
- Has a Primary Key
- Can have infinite number of items (=rows)
- Each item has attributes (can be added over time, can be null)
- Max item size is 400KB
- Data tyes supported:
    - ScalarTypes
    - DocumentTypes
    - SetTypes

### Read/Write Capacity Modes
- Provisioned Mode(default)
    - Specify the number of reads/writes per second
    - Pay for provisioned Read Capacity Units(RCU) & Write (WCU)
    - Possibility to add auto-scaling mode for RCU & WCU
- On-Demand Mode
    - Read/Write automatically scales up/down with your workloads
    - No capacity planning needed
    - Pay what you use, more expensive
    - Great for unpredictable workloads

--- 

## DyanmoDB Accelerator (DAX)
- Fully-managed, highly available, seamless in memory cache for DyanmoDB
- Help solve read congestion by caching
- DAX vs ElastiCache
    - DAX for DynamoDB
    - ElastiCache for others

---

## DynamoDB Stream
- Sent to Kinesis
- Read by Lambda
- Read by Kinesis Client Library apps
- Data Retention for up to 24hrs
- Use cases:
    - React to changes in real-time
    - Analytic
    - Insert to derivative tables
    - Insert into ElastiSearch
    - Implement cross-region replication

---

## DynamoDB Global Tables
- Cross zone table - two-way replication
- Low latency
- Active-active replication
- Apps can READ & WRITE
- Must enable DyanmoDB Streams as a pre-requisite

---

## DynamoDB - Time To Live (TTL)
- Auto delete items ager expiry timestamp
- Use case:
    - Reduce stored data by keep only current items
    - Adhere to regulatory obligations

--- 

## DynamoDB - Indexes
- Allow to query on attributes other than the Primary Key

---

## DynamoDB - Transactions
- A transaction is written to both tables, or more

---

## API Gateway
- Proxy requests to Lambda and DynamoDB or any AWS service
- Can use with Lambda
- REST/WS API proxy requests to lambda
- Fully managed
- Serverless

---

## API Gateway - EndpointTypes
- Edge-Optimized (default):
    - For global clients
- Regional:
    - Clients within the same region
    - Can manually combine with CloudFront
- Private

---

## API Gateway - Security

### IAM Permission
- Create IAM policy for User/Role
- Use Sigv4 capability where IAM credentials are in headers

### Lambda Authorizer
- Uses AWS Lambda to validate the token in header being passed
- Use OAuth/SAML/3rd party type of authentication
- Lambda return IAM policy for user

### Cognito User Pools
- Fully manages user lifecycle
- No custom implementation requiredd
- Only helps with authentication, not authorization

---

## AWS Cognito
- Cognito User Pools(CUP):
    - Sign in functionality for app users
    - Integrated with API Gateway
- Cognito Identity Pools:
    - Provide AWS credentials to users so they can access AWS resources directiy
    - Integrated with Cognito User Pools as an identity provider
- Cognito Sync:
    - Sync data from device to Cognito
    - Deprecated and replaced by AppSync

---

## AWS SAM - Serverless Application Model
- Framework for developing and deploying serverless apps
- All the config is YAML code
    - Lambda function
    - DynamoDB tables
    - API Gateway
    - Cognito User Pools
- SAM can help you to run Lambda, API Gateway, DynamoDB locally
- SAM can use CodeDeploy to deploy Lambda functions
