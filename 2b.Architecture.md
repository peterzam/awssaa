# Architecture

## Placement Group
- Cluster 
    - Low latency group in a single AZ
    - Same Rack
    - Big Data job for fast
    - Extram low latency and high network
- Spread 
    - Spread across underlying hardware 
    - Multiple AZs
    - Max 7 instances per AZ per Group
    - Critical apps
- Partition
    - Similar to Spread
    - But different partitions
    - Partition == Rack
    - 7 partiions per AZ
    - 100 EC2
    - Check status by metadata
    - Big Data, Hadoop, HBasem Cassandra, Kafka

---

## ENI (Elastic Network Interfaces)
- Virtual Network card
- Primary IPv4, one or more
- One Elastic IP per private IPv4
- One Public IPv4
- A MAC address
- Can detach and reattch for failover
- Bound to a specific AZ

---

## EC2 Hibernate
- Hibernate
- Save RAM state under root
- Root of EBS must be encrypted, not instance store & large
- C, M, R types support (Xen & Nitro)
- RAM must less than 150 GB
- Not supported for bare metal
- Linux
- For On-demand & Reserved
- Cannot be hibernated over 60 days
- Cannot hibernate already run instance

---

## EC2 Nitro
- New virtualization technology
- Better performance
    - Better newtorking options (enchanced networking, HPC, IPv6)
    - Higher Speed EBS (Necessary for 64k max EBS IOPS - only 32k max on non-Nitro)

---

## vCPU
- 1 core = 2 threads(vCPU)
- Can request # threads per core

---

## EC2 Capacity Reservation
- Manual or planned end-date
- No need 1 or 3 year commiment
